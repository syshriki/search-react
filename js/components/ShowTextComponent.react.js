var React = require('react');
var ReactDOM = require('react-dom');

var ShowTextComponent = React.createClass({ 
	getInitialState: function(){
		return {"showComplete" : false}
	},
	handleClick: function(event) {
    	this.setState({showComplete:!this.state.showComplete});
    },
	render: function(){	
		var text = this.props.children;
		var maxSize = this.props.maxSize;
		var clickText = this.state.showComplete ? "View Less" : "View More";
		var exceedsMaxSize = text.length > maxSize;
		return (
			<p>
				<span className="show-text-teaser">{text.substring(0, maxSize)}</span>
				<span className={this.state.showComplete ? 'show-text-visible' : 'show-text-hidden'}>{text.substring(maxSize, text.length-1)}</span>
				<span className={exceedsMaxSize ? 'show-text-visible' : 'show-text-hidden'} onClick={this.handleClick}>
				 ...
				 <a>{clickText}</a>
				</span>
			</p>
		);
	}
});

module.exports = ShowTextComponent;
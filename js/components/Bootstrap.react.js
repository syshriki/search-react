var React = require('react');
var Tooltip = require('react-bootstrap/lib/Tooltip');
var OverlayTrigger = require('react-bootstrap/lib/OverlayTrigger');

//Bootstrap constants
const OverlayTooltip = React.createClass({
  render() {
    let tooltip = <Tooltip>{this.props.tooltip}</Tooltip>;

    return (
      <OverlayTrigger
        overlay={tooltip} placement={this.props.placement}
        delayShow={300} delayHide={150}
      >
        {this.props.children}
      </OverlayTrigger>
    );
  }
});


module.exports = Bootstrap
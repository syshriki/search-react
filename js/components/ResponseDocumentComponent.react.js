var React = require('react');
var ReactDOM = require('react-dom');
var Glyphicon = require('react-bootstrap/lib/Glyphicon');
var Collapse = require('react-bootstrap/lib/Collapse');
var Well = require('react-bootstrap/lib/Well');
var Label = require('react-bootstrap/lib/Label');
var _ = require('underscore-node');
var ShowTextComponent = require('./ShowTextComponent.react');
var FluxQueryActions = require('../actions/FluxQueryActions');
var FluxQueryStore = require('../stores/FluxQueryStore');
var FluxResponseActions = require('../actions/FluxResponseActions');
var FluxResponseStore = require('../stores/FluxResponseStore');
var FluxServerActions = require('../actions/FluxServerActions');
var StoreIdTracker = require('../static/StoreIdTracker');

var FieldOpperationsMixin = {
	getFirstValue: function(fieldValue){

		if( Object.prototype.toString.call( fieldValue ) === '[object Array]' ) {
			return fieldValue[0];
		}
	
		return fieldValue;
	},
	getFirstFieldNameWithNotNullValue: function(fieldNames,fields) {
		var fieldNameFound = null;
		_.every(fieldNames,function(fn){
			var fieldValue = fields[fn];
			if(typeof fieldValue !== undefined && fieldValue != null && fieldValue.length > 0){
				fieldNameFound = fn;
				return;
			}
		});	
		return fieldNameFound;
	}
};

/*TODO set max string size on certain fields*/
var ResponseDocument = React.createClass({
	mixins: [FieldOpperationsMixin],
	getInitialState: function(){
		return {fluxId:StoreIdTracker.createId()}
	},
	scrollIntoView: function(){
		ReactDOM.findDOMNode(this).scrollIntoView(true);
	},
	componentWillMount: function(){
    	this.setResponseDocumentData(this.props.fields);
	},
	componentWillReceiveProps: function(nextProps){
		this.setResponseDocumentData(nextProps.fields);
	},
	setResponseDocumentData: function(fields){
		//Names of fields for title and teaser (not always necesarily fields "title" and "teaser")
		var titleFieldName = this.getFirstFieldNameWithNotNullValue(this.props.preferences.responseDocument.titleFieldNames.value,this.props.fields);
		var teaserFieldName = this.getFirstFieldNameWithNotNullValue(this.props.preferences.responseDocument.teaserFieldNames.value,this.props.fields);

		//Actual field values for whatever the title and teaser field are (once again not necesarily the values of "teaser" and "title" field)
		var title = this.getFirstValue(this.props.fields[titleFieldName]);
		var teaser = this.getFirstValue(this.props.fields[teaserFieldName]);

		this.setState({title: title, teaser: teaser, teaserFieldName: teaserFieldName,titleFieldName: titleFieldName})
	},
	render: function() {
		var preferences = this.props.preferences;
		var docid = this.props.fields[".id"];		
		var fields = this.props.fields;
		var teaserFieldName = this.props.fields[teaserFieldName];
	return (
		<div className="response-document">
			<h4>{this.state.title}
				{this.props.preferences.responseDocument.useFieldValueAsSuptext.value || this.props.preferences.responseDocument.useFilenameExtAsSuptext.value ? <TitleSuptext preferences={preferences.responseDocument} fields={fields} /> : ''}
			</h4>
			<div>
				<ShowTextComponent maxSize={this.props.preferences.responseDocument.fields[this.state.teaserFieldName].maxDisplayChars.value}>{this.state.teaser}</ShowTextComponent> 
				<ResponseDocumentFieldList fluxId={this.state.fluxId} docid={docid} preferences={preferences.main} onExpand={this.scrollIntoView}/>
			</div>
		</div>
	);
	}
}); 

var TitleSuptext = React.createClass({ 
	mixins: [FieldOpperationsMixin],
	getFileExtension : function(filename){
		if(typeof filename !== undefined && filename != null || filename.length <= 0){
  			filenameExt = filename.substr((~-filename.lastIndexOf(".") >>> 0) + 2);
  			return filenameExt.length > 0 ? filenameExt : "";
  		}
	},
	render: function(){	
		var supValue = this.props.preferences.useFilenameExtAsSuptext.value ? this.getFileExtension(this.getFirstValue(this.props.fields.filename)) : this.getFirstValue(this.props.fields[this.getFirstFieldNameWithNotNullValue(this.props.preferences.supTextFieldNames.value,this.props.fields)]);
		return (
			<span>
				{supValue!==null ? <sup> [{supValue}] </sup> : ''}
			</span>
		);
	}
});

var ResponseDocumentFieldList = React.createClass({ 
	getInitialState: function() {
		return {open: false};
	},
	displayAllDocumentFields : function(){
		FluxServerActions.executeQuery(this.props.preferences.queryUrl.value,FluxQueryStore.getQueryParams(this.props.fluxId),this.props.fluxId,this.doneLoading);
		this.toggleCollapse();
	},
	componentDidMount: function(){
		var props = this.props
		FluxQueryActions.initQuery(props.fluxId,"main");
		FluxQueryActions.setQueryString(".id:\""+props.docid+"\"",props.fluxId);
		FluxQueryActions.setRows(1,props.fluxId);
		FluxResponseStore.addChangeListener(this._onChange);
	},
	componentWillUnmount: function() {
		FluxResponseStore.removeChangeListener(this._onChange);
	},
	//This is only called when something in the flux query store changes
	_onChange: function(){
		this.setState({documents:FluxResponseStore.getDocuments(this.props.fluxId)})
	},
	toggleCollapse : function(){
		this.setState({ open: !this.state.open });
	},
	render: function(){	
		//typeof this.state.queryResponse !== "undefined" && typeof this.state.queryResponse.documents !="undefined" ?
  		var fieldNodes = typeof this.state.documents !== "undefined" && _.size(this.state.documents)>0 ? _.map(this.state.documents[0].fields,function(value,key) {
			return (
	        	<ResponseDocumentField key={key} field={key} fieldValue={value} />
			);
   		 }) : FluxResponseStore.getStatus(this.props.fluxId)==="executing" ? "Loading please wait..." : "";


	  	var chevronDownClass = this.state.open ? "hide" : "show";
	  	var chevronUpClass = !this.state.open ? "hide" : "collapse-well show";
	  	var scrollIntoView = this.props.onExpand;
    	var fluxId = this.props.fluxId;

    return (
	    <span>
		   	<Glyphicon glyph="chevron-down" className={chevronDownClass} onClick=
			    { 
			    	()=> this.displayAllDocumentFields()
			    } />

		    <Collapse in={this.state.open} onEntered={ 
						    		()=> scrollIntoView()
						    	}>
				<div>
					<Well>
						<span onClick=
						    { 
						    	()=> this.setState({ open: !this.state.open })
						    }>
							<Glyphicon glyph="chevron-up" className={chevronUpClass} />
						</span>
						<table className="table response-document-field-list">
							<tbody>
								{fieldNodes}
							</tbody>
						</table>
						<br />
						<span onClick=
						    { 
						    	()=> this.setState({ open: !this.state.open })
						    }>
							<Glyphicon glyph="chevron-up" className={chevronUpClass} />
						</span>
					</Well>
				</div>
		    </Collapse>
		 </span>
    );
  }
});

var ResponseDocumentField = React.createClass({
  render: function() {
  	var field = this.props.field;
  	var fieldValue = JSON.stringify(this.props.fieldValue);
    return (
	    <tr>
	      <td className="field-label"><h4><Label>{field}</Label></h4></td><td className="field-value">{fieldValue}</td>
	    </tr>
    );
  }
});

module.exports = ResponseDocument;
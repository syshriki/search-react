var React = require('react');
var ReactDOM = require('react-dom');
var Modal = require('react-bootstrap/lib/Modal');
var Button = require('react-bootstrap/lib/Button');
var Tab = require('react-bootstrap/lib/Tab');
var Tabs = require('react-bootstrap/lib/Tabs');
var Row = require('react-bootstrap/lib/Row');
var Col = require('react-bootstrap/lib/Col');
var Nav = require('react-bootstrap/lib/Nav');
var NavItem = require('react-bootstrap/lib/NavItem');

var PreferenceTab = React.createClass({ 
	render: function(){	
		var tabname = this.props.tabname;
		var tabid = this.props.tabid;
		var active = this.props.active;
		return (
			<li className={active ? "active" : ""}><a href={"#"+tabid} data-toggle="tab">{tabname}</a></li>
		);
	}
});

var PreferencePaneField = React.createClass({ 
	render: function(){	
		var fieldName = this.props.fieldName;
		var fieldValue = this.props.fieldValue;
		var tabid = this.props.tabid;
		return (
			<tr><td><label for={tabid+"-content"}>{fieldName}:</label></td><td><input id={tabid+"-content"} type="text" value={fieldValue}/></td></tr>
		);
	}
});


var PreferencePane = React.createClass({ 
	render: function(){	
		var tabid = this.props.tabid;
		var preferences = this.props.preferences;
		var counter = 0;
		var fieldNodes = Object.keys(preferences).map(function(preference) {
			if(preference!=='metadata')
				return (
					<PreferencePaneField key={tabid+preference} fieldid={tabid} fieldName={preferences[preference].displayName} fieldValue={preferences[preference].value}/>
				);
		});

		return (
			<div id={tabid} className="tab-pane fade preference-pane">
				<table>
					<tbody>
						{fieldNodes}
					</tbody>
				</table>
			</div>
		);
	}
});

var PreferencePanes = React.createClass({ 
	render: function(){	
		var counter = 0;
		var preferences = this.props.preferences;
		var paneNodes = Object.keys(preferences).map(function(tabid) {
			counter++;
			return (
				<PreferencePane key={counter+tabid} tabid={tabid} preferences={preferences[tabid]}/>
			);
		});

		return (
			<div className="tab-content">
				{paneNodes}
			</div>
		);
	}
});

var PreferenceTabs = React.createClass({ 
	render: function(){	
		var preferences = this.props.preferences;
		var counter = 0;
		var tabnodes = Object.keys(preferences).map(function(preference) {
			counter++;
			return (
				<PreferenceTab key={counter+"-"+preference} tabname={preferences[preference].metadata.displayName} tabid={preference} active={counter===1} />
			);
		});
		return (
				 <ul className="nav nav-tabs" data-tabs="tabs">
				    {tabnodes}
				 </ul>
		);
	}
});

var PreferencesComponent = React.createClass({ 
	getInitialState : function(){
		return { showModal: false };
	},
	close : function(){
		this.setState({ showModal: false });
	},
	open : function(){
		this.setState({ showModal: true });
	},
	render: function(){	
		var preferences = this.props.preferences;
		var title = this.props.title;
		var text = this.props.children;
		return (
			<span>
				<span onClick={this.open}>{text}</span>
				<Modal show={this.state.showModal} onHide={this.close}>
		          <Modal.Header closeButton>
		            <Modal.Title>{title}</Modal.Title>
		          </Modal.Header>
		          <Modal.Body>
		            	<div className="tabbable">
				            <PreferenceTabs preferences={preferences} />
				            <PreferencePanes preferences={preferences} />
				        </div>
		          </Modal.Body>
		          <Modal.Footer>
		            <Button onClick={this.close}>Close</Button>
		          </Modal.Footer>
		        </Modal>
	        </span>
		);
	}
});

module.exports = PreferencesComponent;
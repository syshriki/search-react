var React = require('react');
var ReactDOM = require('react-dom');
var Glyphicon = require('react-bootstrap/lib/Glyphicon');
var Button = require('react-bootstrap/lib/Button');
var FluxQueryStore = require('../stores/FluxQueryStore');
var FluxQueryActions = require('../actions/FluxQueryActions');
var FluxServerActions = require('../actions/FluxServerActions');

var SearchComponent = React.createClass({
	componentWillMount: function(){
		//Execute initial query (initialized in Sail)
		this.executeNewSearch();
	},
	executeNewSearch: function(){
		FluxQueryActions.setOffset(0,this.props.fluxId);
		FluxServerActions.executeQuery(this.props.queryUrl,FluxQueryStore.getQueryParams(this.props.fluxId),this.props.fluxId);
	},
	_onkeyDown: function(event){
		//If enter is pressed
        if(event.keyCode == 13){
            this.executeNewSearch();
        }
    },
	render: function() {
		console.log("Rendering search controls");
		return (
			<div className="search-controls" onKeyDown={this._onkeyDown}>
				<SearchBox fluxId={this.props.fluxId} queryUrl={this.props.queryUrl}/>
				<Button bsStyle="primary" className="search-button" onClick={this.executeNewSearch}>
					<Glyphicon glyph="search" /> Search
				</Button>
			</div>
		);
	}
});

//** TODO ADD BOOTSTRAP INPUT ADDON TO signify advanced or simple query language **//
var SearchBox = React.createClass({
	getInitialState: function() {
		return FluxQueryStore.getQueryParams(this.props.fluxId);
	},
	componentDidMount: function(){
		FluxQueryStore.addChangeListener(this._onChange);
	},
	componentWillUnmount: function() {
		FluxQueryStore.removeChangeListener(this._onChange);
	},
	//This is only called when something in the flux query store changes
	_onChange: function(){
		this.setState({"query":FluxQueryStore.getQueryString(this.props.fluxId),"queryLanguage":FluxQueryStore.getQueryLanguage(this.props.fluxId)})
	},
	getQueryLanguage: function(query){//Takes query and identifies query language
		if(query === undefined || query == null || query.length <= 0){
			console.log("Query is not set")
			return "simple";
		}
		var lower = query.toLowerCase();
		//Determines if lanuage is advanced
		if(lower.substring(0, 5)==="term("||
			lower.substring(0, 5)==="date("||
			lower.substring(0, 7)==="entity("||
			lower.substring(0, 6)==="scope("||
			lower.substring(0, 6)==="fuzzy("||
			lower.substring(0, 6)==="regex("||
			lower.substring(0, 7)==="isnull("||
			lower.substring(0, 8)==="context("||
			lower.substring(0, 5)==="near("||
			lower.substring(0, 7)==="phrase("||
			lower.substring(0, 4)==="and("||
			lower.substring(0, 3)==="or("||
			lower.substring(0, 4)==="not("||
			lower.substring(0, 6)==="boost("||
			lower.substring(0, 8)==="subquery("||
			lower.substring(0, 6)==="query("||
			lower.substring(0, 5)==="join("||
			lower.substring(0, 14)==="compositejoin("||
			lower.substring(0, 7)==="filter("||
			lower.substring(0, 5)==="near("||
			lower.substring(0, 6)==="onear("||
			lower.substring(0, 7)==="phrase("||
			/^[a-z_]*:endswith/.test(lower)||
			/^[a-z_]*:equals/.test(lower)||
			/^[a-z_]*:startswith/.test(lower))
			return 'advanced';
		else
			return 'simple';
	},
	updateQuery : function(event){
		FluxQueryActions.setQueryString(event.target.value,this.props.fluxId);
		FluxQueryActions.setQueryLanguage(this.getQueryLanguage(event.target.value),this.props.fluxId);
	},
	render: function() {
		return ( 
			<input type="text" id="query" name="query" value={this.state.query} className="search-box" onChange={this.updateQuery}/> 
		);
	}
});

module.exports = SearchComponent;
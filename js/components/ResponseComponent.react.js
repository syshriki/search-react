var React = require('react');
var ReactDOM = require('react-dom');
var ResponseDocument = require('./ResponseDocumentComponent.react');
var FluxResponseStore = require('../stores/FluxResponseStore');
var FluxPreferenceStore = require('../stores/FluxPreferenceStore');
var ListGroupItem = require('react-bootstrap/lib/ListGroupItem');
var ReactCSSTransitionGroup = require('react-addons-css-transition-group');

var ResponseComponent = React.createClass({
	render: function() {
		return (
			<div>
				<ResponseStatus fluxId={this.props.fluxId} preferences={this.props.preferences}/>
				<ResponseDocumentList preferences={this.props.preferences} fluxId={this.props.fluxId}/>
			</div>
		);
	}
}); 

var ResponseDocumentList = React.createClass({
	getInitialState: function() {
		return {"documents":FluxResponseStore.getDocuments(this.props.fluxId)};
	},
	componentDidMount: function(){
		FluxResponseStore.addChangeListener(this._onChange);
	},
	componentWillUnmount: function() {
		FluxResponseStore.removeChangeListener(this._onChange);
	},
	_onChange: function(){
		this.setState({"documents":FluxResponseStore.getDocuments(this.props.fluxId)})
	},
	render: function() {
		var preferences = this.props.preferences;
		var documentNodes = typeof this.state.documents !== "undefined" ? this.state.documents.map(function(document) {
			var fields = document.fields;
			return (
				<ResponseDocument fields={fields} preferences={preferences} key={fields[".id"]}></ResponseDocument>
			);
		}) : "";
		if(typeof this.state.documents !== "undefined" && this.state.documents.length===0)
			documentNodes = "No documents found";

		return (
			<div className="response-document-list">
					{documentNodes}
			</div>
		);
	}
}); 

var ResponseMetaData = React.createClass({
	getInitialState: function() {
		return {"totalTime":FluxResponseStore.getTotalTime(this.props.fluxId),"totalHits":FluxResponseStore.getTotalHits(this.props.fluxId)};
	},
	componentDidMount: function(){
		FluxResponseStore.addChangeListener(this._onChange);
	},
	componentWillUnmount: function() {
		FluxResponseStore.removeChangeListener(this._onChange);
	},
	_onChange: function(){
		this.setState({"totalHits":FluxResponseStore.getTotalHits(this.props.fluxId),"totalTime":FluxResponseStore.getTotalTime(this.props.fluxId)})
	},
	render: function() {
		return (
			<div className="response-metadata">
				{this.state.totalHits} results in {this.state.totalTime}ms 
			</div>
		);
	}
}); 

var ResponseStatus = React.createClass({
	getInitialState: function() {
		return {"status":FluxResponseStore.getStatus(this.props.fluxId)};
	},
	componentDidMount: function(){
		FluxResponseStore.addChangeListener(this._onChange);
	},
	componentWillUnmount: function() {
		FluxResponseStore.removeChangeListener(this._onChange);
	},
	_onChange: function(){
		this.setState({"status":FluxResponseStore.getStatus(this.props.fluxId)})
	},
	render: function() {
		var fluxId = this.props.fluxId;
		var content = "";
		var statusClass = "default"; 

		if(this.state.status==="failed"){ 
			statusClass = "danger";
			content = "Could not connect to query server "+this.props.main.queryUrl.value;
		}else if(this.state.status==="none"){
			statusClass = "danger";
			content = "Unknown error";
		}
		else if(this.state.status==="executing") {
			statusClass = "info";
			content = "Loading, please wait...";
		}
		else if(this.state.status==="complete"){ 
			statusClass = "success";
			content = <ResponseMetaData key={this.state.status} fluxId={fluxId}/>;
		}

		return (
			 <span>
	          	<ListGroupItem key={this.state.status} bsStyle={statusClass}>{content}</ListGroupItem>
			</span>
		);
	}
}); 

module.exports = ResponseComponent;
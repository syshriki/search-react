var React = require('react');

var Footer = React.createClass({
	render: function() {
		var year = new Date().getFullYear();
		return (
			<footer id="footer">
				<p>All rights reserved.</p>
			</footer> 
    	);
 	 }
});

module.exports = Footer;
var React = require('react');
var PreferencesComponent = require('../PreferencesComponent.react');
var FluxPreferenceStore = require('../../stores/FluxPreferenceStore');

var Header = React.createClass({
  render: function() {
  	var preferences = this.props.preferences;
    return (
    <header className="attivio-masthead">
			<div className="attivio-header">
				<a><img src={'./img/attivio-logo-reverse.png'} alt="Attivio Search Home" className="attivio-logo"/></a>
				<span className="attivio-identifier">Immediate Visibility Into All Information</span>
				<ul className="attivio-toolbar list-inline">
		        	<li><span className="masthead-user">shlomo.shriki</span></li>
		        	<li><PreferencesComponent preferences={preferences} title="Global Preferences"><b>Preferences</b></PreferencesComponent></li>
		      	</ul>
			</div>
	</header>
    );
  }
});

module.exports = Header;
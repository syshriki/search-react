var React = require('react');
var ReactDOM = require('react-dom');
var Update = require('react-addons-update');
window.$ = window.jQuery = require('jquery')
require('bootstrap');
var Col = require('react-bootstrap/lib/Col');
var Row = require('react-bootstrap/lib/Row');
var Grid = require('react-bootstrap/lib/Grid');
var SearchComponent = require('../SearchComponent.react');
var ResponseComponent = require('../ResponseComponent.react');
var PageNavigator = require('../PageNavigatorComponent.react');


var Main = React.createClass({
	//TODO ADD RELEVANCY MODEL
	render: function() {
		var preferences = this.props.preferences;
	    return (
	    	<Grid fluid>
		    	<Row>
					<Col sm={2}></Col>
			   		<Col sm={8}> 
			   			<div className="attivio-landing-card">			   					
					   		<SearchComponent queryUrl={preferences.main.queryUrl.value} resultsPerPage={preferences.main.resultsPerPage.value} fluxId={this.props.fluxId}/>
						</div> 	
					</Col>
					<Col sm={2}></Col>
				</Row>
				<Row>
				    <Col sm={2}>.col-sm-2</Col>
				    <Col sm={8}>
				    	<ResponseComponent preferences={preferences} fluxId={this.props.fluxId}/>
				    	<PageNavigator queryUrl={preferences.main.queryUrl.value} resultsPerPage={preferences.main.resultsPerPage.value} fluxId={this.props.fluxId}/>
				    </Col>
				    <Col sm={2}>.col-sm-2</Col>
				</Row>
			</Grid>
       );
	}
});   


module.exports = Main;
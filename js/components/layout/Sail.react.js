var React = require('react');
var ReactDOM = require('react-dom');

var Header = require('./Header.react')
var Main = require('./Main.react')
var Footer = require('./Footer.react')
var FluxQueryActions = require('../../actions/FluxQueryActions');
var FluxPreferenceActions = require('../../actions/FluxPreferenceActions');
var FluxPreferenceStore = require('../../stores/FluxPreferenceStore');
var StoreIdTracker = require('../../static/StoreIdTracker');

var fluxId = StoreIdTracker.createId();

//"main" is a preset preference identifier for the main page, see (/utils/Prsets.js)
//The main search query is initialized (not executed) as are the main global preferences
FluxPreferenceActions.initPreferences(fluxId,"main");
FluxQueryActions.initQuery(fluxId,"main");


//TODO <Footer />
var SearchApp = React.createClass({
  getInitialState: function() {
	return {"preferences" : FluxPreferenceStore.getGlobalPreferences(fluxId)};
  },
  render: function() {
    return (
    <div>
	    <Header fluxId={fluxId} preferences={this.state.preferences}/>
	    <Main fluxId={fluxId} preferences={this.state.preferences}/>
	 </div>
    );
  }
});

module.exports = SearchApp;
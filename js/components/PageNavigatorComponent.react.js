var React = require('react');
var ReactDOM = require('react-dom');
var FluxServerActions = require('../actions/FluxServerActions');
var FluxResponseStore = require('../stores/FluxResponseStore');
var FluxPreferenceStore = require('../stores/FluxPreferenceStore');
var FluxQueryActions = require('../actions/FluxQueryActions');
var FluxQueryStore = require('../stores/FluxQueryStore');
var Glyphicon = require('react-bootstrap/lib/Glyphicon');
var _ = require('underscore-node');

//** TODO ADD BOOTSTRAP PAGE NAVIGATION **//
var PageNavigator = React.createClass({
	getInitialState: function(){
		return {"totalHits" : FluxResponseStore.getTotalHits(this.props.fluxId),"offset" : FluxQueryStore.getOffset(this.props.fluxId)}
	},
	componentDidMount: function(){
		FluxResponseStore.addChangeListener(this._onChange);
	},
	componentWillUnmount: function(){
		FluxResponseStore.removeChangeListener(this._onChange);
	},
	//This is only called when something in the flux query store changes
	_onChange: function(){
		this.setState({"totalHits" : FluxResponseStore.getTotalHits(this.props.fluxId),"offset" : FluxQueryStore.getOffset(this.props.fluxId)});
	},
	render: function() {
		var totalHits = this.state.totalHits;
		var rows = this.props.resultsPerPage;
		var queryUrl = this.props.queryUrl;
		var offset = this.state.offset;
		var currentPage = offset/rows + 1;
		var fluxId = this.props.fluxId
		var pageNodes = _.map([1,2,3,4,5,6,7,8,9,10], function(i){
			var page = Math.floor(i + (currentPage >= 6 ? currentPage-6 : 0));
			if(totalHits<=0 || (page)*rows>totalHits)
				return;
			else if(page === currentPage){
				return ( <b key={page} className="page">{currentPage}</b> );
			}
			else if(currentPage*rows <= totalHits){
				return ( <a key={page} className="page" onClick={()=>{
					FluxQueryActions.setOffset((page-1)*rows,fluxId);
					FluxServerActions.executeQuery(queryUrl,FluxQueryStore.getQueryParams(fluxId),fluxId);
				}
				}>{page}</a> );
			}
	});  
  	return (
  		<span className="page-navigator">
  		  	{
  		  		currentPage > 1 && pageNodes.length > 1 ? <a key="previousPage" className="page" onClick={()=>{ 
	  		  		FluxQueryActions.setOffset((currentPage-2)*rows,fluxId);
					FluxServerActions.executeQuery(queryUrl,FluxQueryStore.getQueryParams(fluxId),fluxId);
				}
			}>
			<Glyphicon glyph="chevron-left" /> Previous</a> : ""}
  			{FluxResponseStore.getStatus(fluxId) === "complete" ? pageNodes : ''}

  			{
  				(offset + rows < totalHits) ? <a key="nextPage" className="page" onClick={()=>{ 
  		  			FluxQueryActions.setOffset((currentPage)*rows,fluxId);
					FluxServerActions.executeQuery(queryUrl,FluxQueryStore.getQueryParams(fluxId),fluxId);
				}
			}>
			Next <Glyphicon glyph="chevron-right" /></a> : ""}
  		</span>
  	);
  }
});

module.exports = PageNavigator;
var React = require('react');
var ReactDOM = require('react-dom');

var FieldOpperationsMixin = {
	getFirstValue: function(fieldValue){

		if( Object.prototype.toString.call( fieldValue ) === '[object Array]' ) {
			return fieldValue[0];
		}
	
		return fieldValue;
	},
	getFirstFieldNameWithNotNullValue: function(fieldNames,fields) {
		var fieldNameFound = null;
		_.every(fieldNames,function(fn){
			var fieldValue = fields[fn];
			if(typeof fieldValue !== undefined && fieldValue != null && fieldValue.length > 0){
				fieldNameFound = fn;
				return;
			}
		});	
		return fieldNameFound;
	}
};

var PreferencesHelperMixin = {
	getPropertyValue : function(prefernces,tab,property){
		return preferences[tab].property.value;
	},
	getPropertyName : function(prefernces,tab,property){
		return preferences[tab].property.value;
	}
}

module.exports = SailMixins;
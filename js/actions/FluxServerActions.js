var AppDispatcher = require('../dispatcher/AppDispatcher');
var FluxResponseConstants = require('../constants/FluxResponseConstants');
var SearchClient = require('../utils/SearchClient.js');

// Define actions object
var FluxServerActions = {
 executeQuery: function(url,query,id){
    AppDispatcher.dispatch({
      actionType: FluxResponseConstants.QUERY_EXECUTE,
      id: id
    });
    SearchClient.executeQuery(url,query,id);
  }
};

module.exports = FluxServerActions;
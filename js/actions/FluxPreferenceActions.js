var AppDispatcher = require('../dispatcher/AppDispatcher');
var FluxPreferenceConstants = require('../constants/FluxPreferenceConstants');

// Define actions object
var FluxPreferenceActions = {
  setPreference: function(preference,id){
    AppDispatcher.dispatch({
      actionType: FluxPreferenceConstants.PREFERENCE_SET,
      preference: preference,
      id: id
    })
  },
  setPreferences: function(preferences,id){
    AppDispatcher.dispatch({
      actionType: FluxPreferenceConstants.PREFERENCES_SET,
      preferences: preferences,
      id: id
    })
  },
  initPreferences: function(id,presetId){
    AppDispatcher.dispatch({
      actionType: FluxPreferenceConstants.PREFERENCES_INIT,
      id: id,
      presetId: presetId
    })
  }
};

module.exports = FluxPreferenceActions;

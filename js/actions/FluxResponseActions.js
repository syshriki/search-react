var AppDispatcher = require('../dispatcher/AppDispatcher');
var FluxResponseConstants = require('../constants/FluxResponseConstants');
var SearchClient = require('../utils/SearchClient.js');

// Define actions object
var FluxResponseActions = {
  setResponse: function(response,id){
    AppDispatcher.dispatch({
      actionType: FluxResponseConstants.RESPONSE_SET,
      response: response,
      id: id
    })
  }
};

module.exports = FluxResponseActions;

var AppDispatcher = require('../dispatcher/AppDispatcher');
var FluxQueryConstants = require('../constants/FluxQueryConstants');

// Define actions object
var FluxQueryActions = {

  // Receive inital product data
  setQueryString: function(queryString,id) {
    AppDispatcher.dispatch({
      actionType: FluxQueryConstants.QUERYSTRING_SET,
      data: queryString,
      id: id
    })
  },

  setQueryLanguage: function(queryLanguage,id) {
    AppDispatcher.dispatch({
      actionType: FluxQueryConstants.QUERYLANGUAGE_SET,
      data: queryLanguage,
      id: id
    })
  },

  setRows: function(rows,id) {
    AppDispatcher.dispatch({
      actionType: FluxQueryConstants.ROWS_SET,
      data: rows,
      id: id
    })
  },

  setWorkflow: function(workflow,id) {
    AppDispatcher.dispatch({
      actionType: FluxQueryConstants.WORKFLOW_SET,
      data: workflow,
      id: id
    })
  },

  setFacets: function(facets,id) {
    AppDispatcher.dispatch({
      actionType: FluxQueryConstants.FACETS_SET,
      data: facets,
      id: id
    })
  },

  setFields: function(fields,id) {
    AppDispatcher.dispatch({
      actionType: FluxQueryConstants.FIELDS_SET,
      data: fields,
      id: id
    })
  },

  setLocale: function(locale,id) {
    AppDispatcher.dispatch({
      actionType: FluxQueryConstants.LOCALE_SET,
      data: locale,
      id: id
    })
  },

  setSort: function(sort,id) {
    AppDispatcher.dispatch({
      actionType: FluxQueryConstants.SORT_SET,
      data: sort,
      id: id
    })
  },

  setOffset: function(offset,id) {
    AppDispatcher.dispatch({
      actionType: FluxQueryConstants.OFFSET_SET,
      data: offset,
      id: id
    })
  },

  addField: function(field,id) {
    AppDispatcher.dispatch({
      actionType: FluxQueryConstants.FIELD_ADD,
      data: field,
      id: id
    })
  },

  addFacet: function(facet,id) {
    AppDispatcher.dispatch({
      actionType: FluxQueryConstants.FACET_ADD,
      data: facet,
      id: id
    })
  },

  setUrl: function(url,id) {
    AppDispatcher.dispatch({
      actionType: FluxQueryConstants.URL_ADD,
      data: url,
      id: id
    })
  },

  setQueryParams: function(params,id) {
    AppDispatcher.dispatch({
      actionType: FluxQueryConstants.URL_ADD,
      data: params,
      id: id
    })
  },

  initQuery: function(id,presetId){
    AppDispatcher.dispatch({
      actionType: FluxQueryConstants.QUERY_INIT,
      id: id,
      presetId: presetId
    })
  }
};

module.exports = FluxQueryActions;

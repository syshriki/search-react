var keyMirror = require('fbjs/lib/keyMirror');

// Define action constants
module.exports = keyMirror({
  QUERYSTRING_SET: null,
  QUERYLANGUAGE_SET: null,
  ROWS_SET: null,
  WORKFLOW_SET: null,
  FACETS_SET: null,
  FIELDS_SET: null,
  LOCALE_SET: null,
  SORT_SET: null,
  OFFSET_SET: null,
  FIELD_ADD: null,
  FACET_ADD: null,
  URL_ADD: null,
  QUERYPARAMS_SET: null,
  QUERY_INIT: null
});
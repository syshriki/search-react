var keyMirror = require('fbjs/lib/keyMirror');

// Define action constants
module.exports = keyMirror({
  PREFERENCE_SET: null,
  PREFERENCES_SET: null,
  PREFERENCES_INIT: null
});
var keyMirror = require('fbjs/lib/keyMirror');

// Define action constants
module.exports = keyMirror({
  QUERY_EXECUTE: null,
  RESPONSE_SET: null
});
//You can add presets here, there are two that I have added. One for the main query and one for the main preferences.

module.exports = {
  // Load default attivio query
  defaultQuery: function(presetId) {
    var queries = { 
      main:{
      	query: '*:*',
      	queryLanguage:'simple',
        restParams : {
          offset : [0]
        }
      }
    };
    return queries[presetId];
  },

  defaultPreference: function(presetId) {
    var preferences = {
      main :{
        preferences: {
          local: {

          },
          global:{
            main:{
              metadata:{
                displayName:"Main"
              },
              queryUrl: {
                displayName:"Query Url",
                value:"http://localhost:17000/rest/searchApi/search"
              },
              resultsPerPage:{
                displayName:"Results Per Page",
                value: 10
              },
            },
            responseDocument:{
              metadata:{
                displayName: "Response Document"
              },
              useFieldValueAsSuptext:{
                displayName:"Use Field Value as Sup",
                value:false
              },
              useFilenameExtAsSuptext:{
                displayName:"Field Value as Sup",
                value:true
              },
              supTextFieldNames:{
                displayName:"Sup Text Field Names",
                value:["filename"]
              },
              titleFieldNames:{ 
                displayName:"Title Field Names",
                value:["title"]
              },
              teaserFieldNames:{
                displayName:"Teaser Field Names",
                value:["teaser"]
              },
              fields:{
                teaser:{
                     showDisplayName:false,
                     displayName:"Teaser",
                     maxDisplayChars:200,
                     fieldName:["teaser"]//fall back
                }
              }
            }
          }
        }
      }
    };
    return preferences[presetId];
  }

};
//This is for keeping track of components registered in the Query Store so the same id is not registered twice
var queryStoreComponentIds = {};

//Find an id that is not taken and add it
function createId(){
	return queryStoreComponentIds[_.size(queryStoreComponentIds)]=true;
}

function removeId(id){
	queryStoreComponentIds[id]=false;
}

module.exports = StoreIdTracker;
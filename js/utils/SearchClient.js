
var FluxResponseActions = require('../actions/FluxResponseActions');

module.exports = {
	executeQuery: function(url,query,fluxId){
	  $.ajax({
	    type: 'POST',
	    url: url,
	    contentType: "application/json",
	    data: JSON.stringify(query),
	    success: function(data) {
	      //Since a action is being called inside an action, timeout ofsetts this issue
		    setTimeout(function() {
				FluxResponseActions.setResponse(data,fluxId);
			}, 0);
	    },
	    error:function(jqXHR, textStatus, errorThrown) {
	    	setTimeout(function() {
				FluxResponseActions.setResponse(null,fluxId);
			}, 0);	
	    }.bind(this)
	  });
	}
	

};
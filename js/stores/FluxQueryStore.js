var AppDispatcher = require('../dispatcher/AppDispatcher');
var EventEmitter = require('events').EventEmitter;
var FluxQueryConstants = require('../constants/FluxQueryConstants');
var ResponseStore = require('./FluxResponseStore');
var assign = require('object-assign');
var Presets = require('../utils/Presets')

// Define initial data points
var _queries = {};

// Method to load query data from DefaultAttivioQuery
function initQuery(id,presetId){
  _queries[id] = {q :{}};
  _queries[id].q = Presets.defaultQuery(presetId);
}

function setQueryString(queryString,id){
  //We need to escape backslashes
  queryString = queryString.replace(/\\/g, "\\\\");
	_queries[id].q.query = queryString;

}

function setQueryLanguage(queryLanguage,id){
	_queries[id].q.queryLanguage = queryLanguage;
}

function setRows(rows,id){
	_queries[id].q.rows = rows;
}

function setWorkflow(workflow,id){
	_queries[id].q.workflow = workflow;
}

function setFacets(facets,id){
	_queries[id].q.facets = facets;
}

function setFields(fields,id){
	_queries[id].q.fields = fields;
}

function setLocale(locale,id){
	_queries[id].q.locale = locale;
}

function setSort(sort,id){
	_queries[id].q.sort = sort;
}

function setOffset(offset,id){
	_queries[id].q.restParams.offset[0] = offset;
}

function addField(field,id){
	_queries[id].q.fields.push(field)
}

function addFacet(facet,id){
	_queries[id].q.facets.push(facet);
}

function setQueryParams(params){
	_queries[id].q = params;
}

// Extend QueryStore with EventEmitter to add eventing capabilities
var QueryStore = assign({}, EventEmitter.prototype, {

  getQueryString: function(id) {
    return _queries[id].q.query;
  },

  getQueryLanguage: function(id) {
    return _queries[id].q.queryLanguage;
  },

  getRows: function(id) {
    return _queries[id].q.rows;
  },

  getWorkflow: function(id) {
    return _queries[id].q.workflow;
  },

  getFacets: function(id) {
    return _queries[id].q.facets;
  },

  getFields: function(id) {
    return _queries[id].q.fields;
  },

  getLocale: function(id) {
    return _queries[id].q.locale;
  },

  getSort: function(id) {
    return _queries[id].q.sort;
  },

  getOffset: function(id) {
    return _queries[id].q.restParams.offset[0];
  },

  getQueryParams: function(id){
  	return _queries[id].q;
  },

  // Emit Change event
  emitChange: function() {
    this.emit('change');
  },

  // Add change listener
  addChangeListener: function(callback) {
    this.on('change', callback);
  },

  // Remove change listener
  removeChangeListener: function(callback) {
    this.removeListener('change', callback);
  }
});

AppDispatcher.register(function(action) {
    AppDispatcher.waitFor([ResponseStore.dispatchToken]);
    switch(action.actionType) {
    //Query settings
      case FluxQueryConstants.QUERYSTRING_SET:
        setQueryString(action.data,action.id);
        QueryStore.emitChange();
      break;

      case FluxQueryConstants.QUERYLANGUAGE_SET:
        setQueryLanguage(action.data,action.id);
      break;

      case FluxQueryConstants.ROWS_SET:
        setRows(action.data,action.id);
      break;

      case FluxQueryConstants.WORKFLOW_SET:
        setWorkflow(action.data,action.id);
      break;

      case FluxQueryConstants.FACETS_SET:
        setFacets(action.data,action.id);
      break;

      case FluxQueryConstants.FIELDS_SET:
        setFields(action.data,action.id);
      break;

      case FluxQueryConstants.LOCALE_SET:
        setLocale(action.data,action.id);
      break;

      case FluxQueryConstants.SORT_SET:
        setSort(action.data,action.id);
      break;

      case FluxQueryConstants.OFFSET_SET:
        setOffset(action.data,action.id);
        QueryStore.emitChange();
      break;

      case FluxQueryConstants.FIELD_ADD:
        addField(action.data,action.id);
      break;

      case FluxQueryConstants.FACET_ADD:
        addFacet(action.data,action.id);
      break;

      case FluxQueryConstants.FACET_ADD:
        addFacet(action.data,action.id);
      break;

      case FluxQueryConstants.QUERYPARAMS_SET:
      	setQueryParams(action.params);
      break;

      case FluxQueryConstants.FACET_ADD:
        addFacet(action.data,action.id);
      break;

      case FluxQueryConstants.QUERY_INIT:
        initQuery(action.id,action.presetId);
      break;
    }

    return true; // No errors. Needed by promise in Dispatcher.
});


QueryStore.setMaxListeners(0);


module.exports = QueryStore;
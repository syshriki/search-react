var AppDispatcher = require('../dispatcher/AppDispatcher');
var EventEmitter = require('events').EventEmitter;
var FluxPreferenceConstants = require('../constants/FluxPreferenceConstants');
var assign = require('object-assign');
var Presets = require('../utils/Presets')

// Define initial data points
var _preferences = {};

// Method to load query data from DefaultAttivioQuery

function setPreference(preference,id){
  _preferences[id][preference] = preference;
}

function setPreferences(preference,id){
  _preferences[id] = preferences;
}

function initPreferences(id,presetId){
  _preferences[id] = Presets.defaultPreference(presetId);
}

// Extend ResponseStore with EventEmitter to add eventing capabilities
var PreferenceStore = assign({}, EventEmitter.prototype, {

  getGlobalPreferences: function(id){
    return typeof _preferences[id] === 'undefined' ? [] : _preferences[id].preferences.global;
  },
  getGlobalPreference: function(preference,id){
    return typeof _preferences[id].preferences[preference] === 'undefined' ? null : _preferences[id].preferences.global[preference].value;
  },
  getLocalPreferences: function(id){
    return typeof _preferences[id] === 'undefined' ? [] : _preferences[id].preferences.local;
  },
  getLocalPreference: function(preference,id){
    return typeof _preferences[id].preferences[preference] === 'undefined' ? null : _preferences[id].preferences.local[preference].value;
  },
  // Emit Change event
  emitChange: function() {
    this.emit('change');
  },
  // Add change listener
  addChangeListener: function(callback) {
    this.on('change', callback);
  },

  // Remove change listener
  removeChangeListener: function(callback) {
    this.removeListener('change', callback);
  }
});

AppDispatcher.register(function(action) {
    var text;
    switch(action.actionType) {
      case FluxPreferenceConstants.PREFERENCES_SET:
        setPreferences(action.preferences,action.id);
        PreferenceStore.emitChange();
      break;

      case FluxPreferenceConstants.PREFERENCE_SET:
        setPreference(action.preference,action.id);
        PreferenceStore.emitChange();
      break;

      case FluxPreferenceConstants.PREFERENCES_INIT:
        initPreferences(action.id,action.presetId);
        PreferenceStore.emitChange();
      break;
    }

    return true; // No errors. Needed by promise in Dispatcher.
});

PreferenceStore.setMaxListeners(0);

module.exports = PreferenceStore;
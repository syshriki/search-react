var AppDispatcher = require('../dispatcher/AppDispatcher');
var EventEmitter = require('events').EventEmitter;
var FluxResponseConstants = require('../constants/FluxResponseConstants');
var assign = require('object-assign');
var Presets = require('../utils/Presets')


// Define initial data points
var _responses = {};

// Method to load query data from DefaultAttivioQuery

function setResponse(response,id){
  _responses[id] = response;
}

// Extend ResponseStore with EventEmitter to add eventing capabilities
var ResponseStore = assign({}, EventEmitter.prototype, {
  getTotalTime: function(id) {
    return typeof _responses[id] === 'undefined' ? -1 : _responses[id].totalTime;
  },

  getTotalHits: function(id){
  	return typeof _responses[id] === 'undefined' ? -1 : _responses[id].totalHits;
  },

  getDocuments: function(id){
    return typeof _responses[id] === 'undefined' ? [] : _responses[id].documents;
  },
  getStatus: function(id){
    return typeof _responses[id] === 'undefined' ? "none" : _responses[id].status;
  },
  // Emit Change event
  emitChange: function() {
    this.emit('change');
  },
  // Add change listener
  addChangeListener: function(callback) {
    this.on('change', callback);
  },

  // Remove change listener
  removeChangeListener: function(callback) {
    this.removeListener('change', callback);
  },

});

ResponseStore.dispatchToken = AppDispatcher.register(function(action) {
  var text;

  switch(action.actionType) {
    case FluxResponseConstants.RESPONSE_SET:
      setTimeout(function() {
        if(action.response!==null)
            setResponse(action.response,action.id);
        else
          _responses[action.id] = {};
        _responses[action.id].status = action.response===null ? "failed" : "complete";
        ResponseStore.emitChange();
      },0);
    break;

    case FluxResponseConstants.QUERY_EXECUTE:
      _responses[action.id] = {};
      _responses[action.id].status = "executing";
      ResponseStore.emitChange();
    break;
  }

  return true; // No errors. Needed by promise in Dispatcher.
})


ResponseStore.setMaxListeners(0);

module.exports = ResponseStore;
var React = require('react');
var ReactDOM = require('react-dom');
var Sail = require('./components/layout/Sail.react');
var FluxQueryActions = require('./actions/FluxQueryActions');
var _ = require('underscore-node');

ReactDOM.render(
  <Sail />,
  document.getElementById('content')
);
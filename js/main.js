var React = require('react');
var ReactDOM = require('react-dom');
var Update = require('react-addons-update');
window.$ = window.jQuery = require('jquery')
require('bootstrap');
var _ = require('underscore-node');
var Tooltip = require('react-bootstrap/lib/Tooltip');
var OverlayTrigger = require('react-bootstrap/lib/OverlayTrigger');
var Button = require('react-bootstrap/lib/Button');
var Glyphicon = require('react-bootstrap/lib/Glyphicon');
var Col = require('react-bootstrap/lib/Col');
var Row = require('react-bootstrap/lib/Row');
var Grid = require('react-bootstrap/lib/Grid');
var Collapse = require('react-bootstrap/lib/Collapse');
var Well = require('react-bootstrap/lib/Well');
var Label = require('react-bootstrap/lib/Label');

//Bootstrap constants
const OverlayTooltip = React.createClass({
  render() {
    let tooltip = <Tooltip>{this.props.tooltip}</Tooltip>;

    return (
      <OverlayTrigger
        overlay={tooltip} placement={this.props.placement}
        delayShow={300} delayHide={150}
      >
        {this.props.children}
      </OverlayTrigger>
    );
  }
});
//TODO <Footer />
var RootContainer = React.createClass({
  render: function() {
    return (
    <div>
	    <Header />
	    <SearchContainer />
    	
	</div>
    );
  }
});

var Header = React.createClass({
  render: function() {
    return (
    <header className="attivio-masthead">
			<div className="container-fluid">
				<div className="navbar-header">
					<a><img src={'./img/attivio-logo-reverse.png'} alt="Attivio Search Home" className="attivio-logo"/></a>
					<span className="attivio-identifier">Immediate Visibility Into All Information</span>
				</div>
			</div>
	</header>
    );
  }
});

var Footer = React.createClass({
  render: function() {
  	var year = new Date().getFullYear();
    return (
		<footer id="footer">
			<p>All rights reserved.</p>
		</footer> 
    );
  }
});


var SearchContainer = React.createClass({
	getInitialState: function() {
		return {preferences:{responseDocument:{showFileExt:true,teaserFieldNames:["teaser"],titleFieldNames:["title"]},url: 'http://localhost:17000/rest/searchApi/search'},queryResponse:{totalHits:"",totalTime:"",documents:[]},page: 1,resultsPerPage: 10,q:{query:'*:*',queryLanguage:'simple',rows:10,workflow:'search',facets:['table'],fields:['table','title','.id','filename','teaser'],locale:'en',sort:[],restParams:{offset:[0]}}};
	},
	updateDeepStateQueryParams: function(params){
		this.setState({ 
			q: Update(this.state.q, {$merge:params}) 
		});
	},
	goToPage : function(page) {
		this.setState({ 
			q: Update(this.state.q, {$merge:{restParams:{offset:[page*this.state.q.rows]}}}) 
		}, function(){ 
			this.search(); 
		});
	},
	//Does not invoke search method, reassigns query language and query to state
	updateQuery : function(event){
		var query = event.target.value;
		var queryLanguage = this.getQueryLanguage(query);
		this.setState({ 
			q: Update(this.state.q, {$merge:{query:query,queryLanguage:queryLanguage}}) 
		});
	},
	handleSearchClick : function(){
		var params = {restParams:{offset:[0]}};
		if(this.state.q.query==="")
			_.extend(params,{query:"*:*"});
		this.setState({ 
			q: Update(this.state.q, {$merge:params}) 
		},function(){
			this.search();
		});
	},
	search : function() {
		$.ajax({
			type: 'POST',
			url: this.state.preferences.url,
			contentType: "application/json",
			data: JSON.stringify(this.state.q),
			success: function(data) {
				console.log("Searching");
				this.setState({ 
					queryResponse: data
				},function(){console.log("From Inside:"+JSON.stringify(this.state))});
			}.bind(this)
		});
	},
	getQueryLanguage: function(query){//Takes query and identifies query language
		if(query === undefined || query == null || query.length <= 0){
			console.log("Query is not set")
			return "simple";
		}
		var lower = query.toLowerCase();
		//Determines if lanuage is advanced or simple
		if(lower.substring(0, 5)==="term("||
			lower.substring(0, 5)==="date("||
			lower.substring(0, 7)==="entity("||
			lower.substring(0, 6)==="scope("||
			lower.substring(0, 6)==="fuzzy("||
			lower.substring(0, 6)==="regex("||
			lower.substring(0, 7)==="isnull("||
			lower.substring(0, 8)==="context("||
			lower.substring(0, 5)==="near("||
			lower.substring(0, 7)==="phrase("||
			lower.substring(0, 4)==="and("||
			lower.substring(0, 3)==="or("||
			lower.substring(0, 4)==="not("||
			lower.substring(0, 6)==="boost("||
			lower.substring(0, 8)==="subquery("||
			lower.substring(0, 6)==="query("||
			lower.substring(0, 5)==="join("||
			lower.substring(0, 14)==="compositejoin("||
			lower.substring(0, 7)==="filter("||
			lower.substring(0, 5)==="near("||
			lower.substring(0, 6)==="onear("||
			lower.substring(0, 7)==="phrase("||
			/^[a-z_]*:endswith/.test(lower)||
			/^[a-z_]*:equals/.test(lower)||
			/^[a-z_]*:startswith/.test(lower))
			return 'advanced';
		else
			return 'simple';
	},
	//TODO ADD RELEVANCY MODEL
	render: function() {
		console.log("Re-rendering search container");
	    return (
	    	<Grid fluid>
		    	<Row>
					<Col sm={2}></Col>
			   		<Col sm={8}> 
			   			<div className="attivio-landing-card">
							<div className="search-container">
	  							<input type="text" id="query" name="query" value={this.state.q.query} className="search-box" onChange={this.updateQuery} />
					   			<Button bsStyle="primary" className="search-button" onClick={this.handleSearchClick} >
							  		<Glyphicon glyph="search" /> Search
							  	</Button>
							</div>
						</div> 	
					</Col>
					<Col sm={2}></Col>
				</Row>
				<Row>
				    <Col sm={2}>.col-sm-2</Col>
				    <Col sm={8}>
				    	<ResponseMetaData totalTime={this.state.queryResponse.totalTime} totalHits={this.state.queryResponse.totalHits}/>
				    	<ResponseDocumentList queryResponse={this.state.queryResponse} preferences={this.state.preferences}/>
				    	<PageNavigator goToPage={this.goToPage} totalHits={this.state.queryResponse.totalHits} offset={this.state.q.restParams.offset[0]} rows={this.state.q.rows}/>
				    </Col>
				    <Col sm={2}>.col-sm-2</Col>
				</Row>
			</Grid>
       );
	}
});   


var PageNavigator = React.createClass({
	componentDidMount  : function(){
		this.props.goToPage(0);
	},
	render: function() {
		var totalHits = this.props.totalHits;
		var rows = this.props.rows;
		var offset = this.props.offset;
		var goToPage = this.props.goToPage;
		var currentPage = offset/rows;
		//Cant believe this is the only way I could figure out how to do this
		var pageNodes = _.map([0,1,2,3,4,5,6,7,8,9], function(i){
			var page = Math.floor(i + (currentPage >= 6 ? currentPage-6 : 0));
			if(totalHits<=0 || (page+1)*rows>totalHits)
				return;
			else if(page === currentPage){
				return ( <b key={page} className="page"> {currentPage+1} </b> );
			}
			else if(currentPage*rows <= totalHits){
				return ( <a key={page} className="page" onClick={()=>goToPage(page)}> {page+1} </a> );
			}
	});  
  	return (
  		<span className="page-navigator">
  			{pageNodes}
  		</span>
  		);
  }
});

var ResponseDocumentList = React.createClass({
	render: function() {
		var preferences = this.props.preferences;
		var documentNodes = typeof this.props.queryResponse !== "undefined" && typeof this.props.queryResponse.documents !== "undefined" ? this.props.queryResponse.documents.map(function(document) {
			var fields = document.fields;
			return (
				<ResponseDocument fields={fields} preferences={preferences} key={fields[".id"]}></ResponseDocument>
			);
		}) : "No documents found";
		return (
			<div className="response-document-list">
			{documentNodes}
			</div>
		);
	}
}); 

var ResponseMetaData = React.createClass({
	render: function() {
		var totalHits = this.props.totalHits;
		var totalTime = this.props.totalTime;
		return (
			<div className="response-metadata">
				{totalHits} results in {totalTime}ms 
			</div>
		);
	}
}); 

var ResponseDocument = React.createClass({
	getFirstValue: function(fieldValue){
		
		if( Object.prototype.toString.call( fieldValue ) === '[object Array]' ) {
			return fieldValue[0];
		}
	
		return fieldValue;
	},
	getFirstFieldNameWithNotNullValue: function(fieldName,fieldNames,fields) {
		var fieldNameFound = "No "+fieldName+" found";
		_.every(fieldNames,function(fn){
			var fieldValue = fields[fn];
			if(typeof fieldValue !== undefined && fieldValue != null && fieldValue.length > 0){
				fieldNameFound = fn;
				return;
			}
		});	
		return fieldNameFound;
	},
	getFileExtension : function(filename){
		if(typeof filename !== undefined && filename != null || filename.length <= 0){
  			filenameExt = filename.substr((~-filename.lastIndexOf(".") >>> 0) + 2);
  			return filenameExt.length > 0 ? filenameExt : "";
  		}
	},
	scrollIntoView: function(){
		console.log("scrolling true");
		ReactDOM.findDOMNode(this).scrollIntoView(true);
	},
	componentWillMount: function(){
    	this.setResponseDocumentData(this.props.fields);
	},
	componentWillReceiveProps: function(nextProps){
		this.setResponseDocumentData(nextProps.fields);
	},
	setResponseDocumentData: function(fields){
		//Names of fields for title and teaser (not always necesarily fields "title" and "teaser")
		var titleFieldName = this.getFirstFieldNameWithNotNullValue("title",this.props.preferences.responseDocument.titleFieldNames,this.props.fields);
		var teaserFieldName = this.getFirstFieldNameWithNotNullValue("teaser",this.props.preferences.responseDocument.teaserFieldNames,this.props.fields);
		var filenameFieldValue = this.getFirstValue(this.props.fields.filename);

		//Actualy field values for whatever the title and teaser field are (once again not necesarily the values of "teaser" and "title" field)
		var title = this.getFirstValue(this.props.fields[titleFieldName]);
		var teaser = this.getFirstValue(this.props.fields[teaserFieldName]);
		
		//If property is set, add the file extension to the title
		if(this.props.preferences.responseDocument.showFileExt)
			fileExt = <sup>[{this.getFileExtension(filenameFieldValue)}]</sup>;

		this.setState({title: title, teaser: teaser,fileExt: fileExt})
	},
	render: function() {
		var preferences = this.props.preferences;
		var docid = this.props.fields[".id"];		
	return (
		<div className="response-document">
			<h4>{this.state.title}{this.state.fileExt}</h4>
			<div>{this.state.teaser} 
				<ResponseDocumentFieldList docid={docid} preferences={preferences} onExpand={this.scrollIntoView}/>
			</div>
		</div>
	);
	}
}); 

var ResponseDocumentFieldList = React.createClass({ 
	getInitialState: function() {
		return {open: false,loading: false,success: false,attempted: false,q:{query:'*:*',queryLanguage:'simple',rows:1,workflow:'search',facets:[''],fields:['*'],locale:'en',sort:[],restParams:{offset:[0]}}};
	},
	updateDeepStateQueryParams: function(params){
		this.setState({ 
			q: Update(this.state.q, {$merge:params}) 
		},function(){
			console.log(JSON.stringify(this.state.q, {$merge:params}));
		});
		
	},
	displayAllDocumentFields : function(){
		this.setState({ loading: true,attempted: true });
		this.toggleCollapse();
		this.updateDeepStateQueryParams({query:".id:\""+this.props.docid+"\""});
	    this.searchAllDocumentFields();
	},
	toggleCollapse : function(){
		this.setState({ open: !this.state.open });
	},
	searchAllDocumentFields : function(){
		$.ajax({
			type: 'POST',
			url: this.props.preferences.url,
			contentType: "application/json",
			data: JSON.stringify(this.state.q),
			success: function(data) {
				this.setState({queryResponse: data,success: true,loading: false});
			}.bind(this)
			
		});
	},
	render: function(){	
		//typeof this.state.queryResponse !== "undefined" && typeof this.state.queryResponse.documents !="undefined" ?
  		var fieldNodes = typeof this.state.queryResponse !== "undefined" && typeof this.state.queryResponse.documents !="undefined" ? _.map(this.state.queryResponse.documents[0].fields,function(value,key) {
			return (
	        	<ResponseDocumentField key={key} field={key} fieldValue={value} />
			);
   		 }) : "No fields found";

	  	var chevronDownClass = this.state.open ? "hide" : "show";
	  	var chevronUpClass = !this.state.open ? "hide" : "collapse-well show";
	  	var scrollIntoView = this.props.onExpand;

    return (
	    <span>
		   	<Glyphicon glyph="chevron-down" className={chevronDownClass} onClick=
			    { 
			    	()=> this.displayAllDocumentFields()
			    } />

		    <Collapse in={this.state.open} onEntered={ 
						    		()=> scrollIntoView()
						    	}>
				<div>
					<Well>
						<span onClick=
						    { 
						    	()=> this.setState({ open: !this.state.open })
						    }>
							<Glyphicon glyph="chevron-up" className={chevronUpClass} />
						</span>
						{this.state.loading ? 'Loading...' : fieldNodes}
						<br />
						<span onClick=
						    { 
						    	()=> this.setState({ open: !this.state.open })
						    }>
							<Glyphicon glyph="chevron-up" className={chevronUpClass} />
						</span>
					</Well>
				</div>
		    </Collapse>

		 </span>
    );
  }
});

var ResponseDocumentField = React.createClass({
  render: function() {
  	var field = this.props.field;
  	var fieldValue = JSON.stringify(this.props.fieldValue);
    return (
	    <div>
	      <h5><Label>{field}</Label>:{fieldValue}</h5>
	    </div>
    );
  }
});

var Sidebar = React.createClass({
  render: function() {
    return (
    <div>
      side bar content
    </div>
    );
  }
});
ReactDOM.render(
  <RootContainer />,
  document.getElementById('content')
);

var Query = React.createClass({
	getInitialState: function() {
		return {query:'*:*',queryLanguage:'simple',rows:10,workflow:'search',facets:['table'],fields:['table','title','.id','filename','teaser'],locale:'en',sort:[],restParams:{offset:[0]}};
	},
	componentDidMount: function() {
		this.updateQuery();
		this.props.onSearch(this.state);
	},
	componentWillReceiveProps : function() {
		this.updateQuery();
	},
	updateQuery: function(){
		this.setState(
			{
				queryLanguage:this.getQueryLanguage(this.state.query),
				restParams:{offset:[(this.props.page-1)*this.state.rows]},
				rows: this.props.ResultsPerPage
			}
		)
	},
	getQueryLanguage: function(query){//Takes query and identifies query language
		if(query === undefined || query == null || query.length <= 0){
			console.log("Query is not set")
			return;
		}
		var queryLanguage;
		var lower = query.toLowerCase();
		//Determines if lanuage is advanced or simple
		if(lower.substring(0, 5)==="term("||
			lower.substring(0, 5)==="date("||
			lower.substring(0, 7)==="entity("||
			lower.substring(0, 6)==="scope("||
			lower.substring(0, 6)==="fuzzy("||
			lower.substring(0, 6)==="regex("||
			lower.substring(0, 7)==="isnull("||
			lower.substring(0, 8)==="context("||
			lower.substring(0, 5)==="near("||
			lower.substring(0, 7)==="phrase("||
			lower.substring(0, 4)==="and("||
			lower.substring(0, 3)==="or("||
			lower.substring(0, 4)==="not("||
			lower.substring(0, 6)==="boost("||
			lower.substring(0, 8)==="subquery("||
			lower.substring(0, 6)==="query("||
			lower.substring(0, 5)==="join("||
			lower.substring(0, 14)==="compositejoin("||
			lower.substring(0, 7)==="filter("||
			lower.substring(0, 5)==="near("||
			lower.substring(0, 6)==="onear("||
			lower.substring(0, 7)==="phrase("||
			/^[a-z_]*:endswith/.test(lower)||
			/^[a-z_]*:equals/.test(lower)||
			/^[a-z_]*:startswith/.test(lower))
			return 'advanced';
		else
			return 'simple';
	},
  render: function() {
  	return (
	  	<input type="text" id="query" name="query" value={this.state.query} className="search-box" onChange={this.updateQuery} />
  	);
  }
 });

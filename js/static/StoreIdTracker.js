var _ = require('underscore-node');

//This is for keeping track of components registered in the Query Store so the same id is not registered twice
var idCounter = 0;

//Find an id that is not taken and add it
module.exports = {
	createId : function (){
		return idCounter++;
	}
};
